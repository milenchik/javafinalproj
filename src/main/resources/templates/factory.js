var app = angular.module('factory-page', []);

app.controller('FactoryCtrl', function ($scope, $http){
    /* took OrderItems and CustomerOrder */
    $scope.customerOrder = [];
    $scope.getCustomerOrder = function(){
        $http({
            url: 'http://127.0.0.1:8080/customerOrder',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            }
        }).then(function (response){
            $scope.customerOrder = response.data;
            console.log(response);

        }, function (response){
            console.log(response);
        })
    }

    $scope.getCustomerOrder();

    $scope.getOrdersByStatus = function (status) {
        $http({
            url: 'http://127.0.0.1:8080/customerOrder/status/' + status,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.customerOrder = response.data;
        }, function (response){
            console.log(response);
        })

    };

    /* -- change status -- */
    $scope.sent = function (order) {
        $http({
            url: 'http://127.0.0.1:8080/sentOrder',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": order.token
            }
        }).then(function (response){
            console.log(response);
            $scope.getCustomerOrder();
        }, function (response){
            console.log(response);
        })
    };

    /* -- All Products -- */
    $scope.productList = [];
    $scope.getProducts = function () {
        $scope.productList = [];
        $http({
            url: 'http://127.0.0.1:8080/product',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.productList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getCategories = function () {
        $http({
            url: 'http://127.0.0.1:8080/category',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.categoryList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getProductsByCategory = function (categoryID) {
        $http({
            url: 'http://127.0.0.1:8080/product/category/' + categoryID,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.productList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getCategories();
    $scope.getProducts();


    $scope.addQuantity = function (product) {
        $http({
            url: 'http://127.0.0.1:8080/add/product/' + product.productId,
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.getProducts();
        }, function (response){
            console.log(response);
        })

    };
});