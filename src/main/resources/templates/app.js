var app = angular.module('aitu-project', []);

app.controller('ModelCtrl', function ($scope, $http){
    $scope.modelList = [];
    $scope.categoryList = [];
    $scope.getModels = function () {
        console.log("sdsfdfsdf");
        $http({
            url: 'http://127.0.0.1:8080/model',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.modelList = response.data;
        }, function (response){
            console.log("Error");
        })
    };

    $scope.getCategories = function () {
        console.log("sdsfdfsdf");
        $http({
            url: 'http://127.0.0.1:8080/category',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.categoryList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getProductsByCategory = function (categoryID) {
        console.log("sdsfdfsdf");
        $http({
            url: 'http://127.0.0.1:8080/model/category/' + categoryID,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.modelList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getCategories();

    $scope.getModels();
});



app.controller('ProductCtrl', function ($scope, $http){
    $scope.token = '';
    $scope.modelList = [];
    $scope.productList = [];
    $scope.categoryList = [];
    $scope.getProducts = function () {
        $scope.productList = [];
        $http({
            url: 'http://127.0.0.1:8080/product',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.productList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getAllModels = function () {
        $http({
            url: 'http://127.0.0.1:8080/model',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.modelList = response.data;
        }, function (response){
            console.log("Error");
        })
    };

    $scope.getCategories = function () {
        $http({
            url: 'http://127.0.0.1:8080/category',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.categoryList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getProductsByCategory = function (categoryID) {
        $http({
            url: 'http://127.0.0.1:8080/product/category/' + categoryID,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("Success");
            console.log(response);
            $scope.productList = response.data;
        }, function (response){
            console.log("Error");
        })

    };

    $scope.getCategories();

    $scope.getProducts();

    $scope.getAllModels();

    /* -- Creating Order, saving to database -- */
    $scope.detailedOrderProductList = {};
    $scope.messageQuantity = '';
    $scope.addProduct = function (product){
        if ($scope.detailedOrderProductList[product.productId] === undefined) {
            $scope.detailedOrderProductList[product.productId] =  {productId: product.productId, name:product.productName, description: product.productDescription, price: product.price, quantity: 0};
        }
        if (product.quantity < $scope.detailedOrderProductList[product.productId].quantity + 1){
            console.log(product.quantity);
            $scope.messageQuantity = 'There is no any quantity of this product in database!'
        } else {
            $scope.detailedOrderProductList[product.productId].quantity = $scope.detailedOrderProductList[product.productId].quantity + 1;
        }
    }

    $scope.CountTotalPrice = function (){
        $scope.totalPrice = 0;
        angular.forEach($scope.detailedOrderProductList, function (value){
            $scope.totalPrice += value.price * value.quantity;
        });
        console.log($scope.detailedOrderProductList);
    }

    $scope.removeProduct = function (product){
        delete $scope.detailedOrderProductList[product.productId];
    }

    $scope.minusOneQuantity = function (product){
        $scope.detailedOrderProductList[product.productId].quantity = $scope.detailedOrderProductList[product.productId].quantity -1;
        if ($scope.detailedOrderProductList[product.productId].quantity == 0) {
            delete $scope.detailedOrderProductList[product.productId];
        }
    }


    let reorganize = function (){
        $scope.productsID = [];
        $scope.productsQuantity = [];
        angular.forEach($scope.detailedOrderProductList, function (value){
            $scope.productsID.push(value.productId);
            if ($scope.productsQuantity[value.productId] === undefined) {
                $scope.productsQuantity[value.productId] =  value.quantity;
            }
        });
    }

    $scope.messageOrder = '';
    $scope.SendOrderByCourier = function () {
        $scope.CountTotalPrice();
        if (Object.keys($scope.signin_data).length === 0) {
            $scope.messageOrder = 'To make an order you need to authorize!';
        } else {
            if (Object.keys($scope.detailedOrderProductList).length === 0){
                $scope.messageOrder = 'Empty order will not be accepted!';
            } else {
                $scope.messageOrder = 'Your order is accepted!';
                $scope.createCustomerOrder(1);
                console.log($scope.signin_data);
            }
        }
    }

    $scope.SendOrderByPickUp = function () {
        $scope.CountTotalPrice();
        if (Object.keys($scope.signin_data).length === 0) {
            $scope.messageOrder = 'To make an order you need to authorize!';
        } else {
            if (Object.keys($scope.detailedOrderProductList).length === 0){
                $scope.messageOrder = 'Empty order will not be accepted!';
            } else {
                $scope.messageOrder = 'Your order is accepted!';
                $scope.createCustomerOrder(2);
                console.log($scope.signin_data);
            }
        }
    }

    $scope.createCustomerOrder = function (deliveryId) {
        reorganize();
        $http({
            url: 'http://127.0.0.1:8080/customerOrder/create/'+ deliveryId + '/ByCreditCard',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": $scope.signin_data.token
            },
            data: {
                "productIds": $scope.productsID,
                "productQuantity": $scope.productsQuantity,
            }
        }).then(function (response){
            $scope.token = response.data.token;
            console.log($scope.token);
            $scope.detailedOrderProductList = {};
            $scope.getProducts();
            $scope.getCustomerOrder();
        }, function (response){
            console.log(response);
        })
    }

    /* -- Authorization -- */
    $scope.signin_data = {};
    $scope.signIn = function (acc){
        $http({
            url: 'http://127.0.0.1:8080/authorization',
            method: 'POST',
            data: {
                'login': acc.login,
                'password': acc.password
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.signin_data = response.data;
            console.log($scope.signin_data);
            $scope.me();
        }, function (response){
            $scope.signin_data = {};
            console.log(response);
        })
    };

    $scope.customerMessage = '';
    $scope.me = function (){
        $http({
            url: 'http://127.0.0.1:8080/customers/me',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "auth": $scope.signin_data.token
            }
        }).then(function (response){
            $scope.customer = response.data;
            $scope.customerMessage = 'Hello ';
            console.log($scope.customer);
            $scope.getCustomerOrder();
        }, function (response){
            $scope.customer = {};
            $scope.customerMessage = 'Login or Password is incorrect!';
        })
    };

    $scope.signOut = function (){
        $scope.customer = null;
        $scope.signin_data = {};
        $scope.customerMessage = '';
    }

    /* -- Registration -- */
    $scope.register = function (acc){
        $http({
            url: 'http://127.0.0.1:8080/registration',
            method: 'POST',
            data: {
                'login': acc.login,
                'password': acc.password
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.signin_data = response.data;
            registerCustomer(acc);
        }, function (response){
            $scope.signin_data = {};
            console.log(response);
        })
    }

    let registerCustomer = function (acc){
        $http({
            url: 'http://127.0.0.1:8080/registerCustomer',
            method: 'POST',
            data: {
                'organizationName': acc.organizationName,
                'address': acc.address,
                'contactNumber': acc.contactNumber,
                'email': acc.email
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": $scope.signin_data.token
            }
        }).then(function (response){
            console.log(response);
            $scope.me();
        }, function (response){
            console.log(response)
        })
    }

    /* took OrderItems and CustomerOrder */
    $scope.customerOrderOfCustomer = [];
    $scope.getCustomerOrder = function(){
        $http({
            url: 'http://127.0.0.1:8080/customerOrderByCustomerID',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": $scope.signin_data.token
            }
        }).then(function (response){
            $scope.customerOrderOfCustomer = response.data;
            console.log(response);
        }, function (response){
            console.log(response);
        })
    }

});