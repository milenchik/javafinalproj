package aitu.cs.project.repository;

import aitu.cs.project.model.Model;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelRepository extends CrudRepository<Model, Integer> {
    List<Model> findAllByCategoryId(int categoryID);

    Model findByModelId(int id);
}
