package aitu.cs.project.repository;

import aitu.cs.project.model.Model;
import aitu.cs.project.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findAllByModelId(int modelID);

    Product findByProductId(int id);

    Product findByModelId(int id);
}
