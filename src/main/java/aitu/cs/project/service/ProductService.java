package aitu.cs.project.service;

import aitu.cs.project.model.Model;
import aitu.cs.project.model.Product;
import aitu.cs.project.repository.FactoryRepository;
import aitu.cs.project.repository.ModelRepository;
import aitu.cs.project.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final ModelRepository modelRepository;
    private final FactoryRepository factoryRepository;

    public ProductService(ProductRepository productRepository, ModelRepository modelRepository, FactoryRepository factoryRepository) {
        this.productRepository = productRepository;
        this.modelRepository = modelRepository;
        this.factoryRepository = factoryRepository;
    }

    public List<Product> getAll(){
        List<Product> productList = (List<Product>) productRepository.findAll();
        for (Product product:productList) {
            product.setModel(modelRepository.findByModelId(product.getModelId()));
            product.setFactory(factoryRepository.findByFactoryId(product.getFactoryId()));
        }
        return productList;
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(productRepository.findById(id));
    }

    public void deleteByID(int id){
        productRepository.deleteById(id);
    }

    public Product save(Product product){
        return productRepository.save(product);
    }

    public List<Product> getByModelID(int modelID){
        return productRepository.findAllByModelId(modelID);
    }

    public List<Product> getByCategoryID(int categoryID){
        List<Model> modelList = modelRepository.findAllByCategoryId(categoryID);
        List<Product> productList = new ArrayList<Product>();
        for (Model model: modelList) {
            productList.add(productRepository.findByModelId(model.getModelId()));
        }

        for (Product product:productList) {
            product.setModel(modelRepository.findByModelId(product.getModelId()));
            product.setFactory(factoryRepository.findByFactoryId(product.getFactoryId()));
        }
        return productList;
    }

    public Product addProduct(int productId) {
        Product product = productRepository.findByProductId(productId);
        product.setQuantity(product.getQuantity()+1);
        productRepository.save(product);
        return product;
    }
}
