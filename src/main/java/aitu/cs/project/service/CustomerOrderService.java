package aitu.cs.project.service;

import aitu.cs.project.model.*;
import aitu.cs.project.repository.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CustomerOrderService {
    private final CustomerOrderRepository customerOrderRepository;
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;
    private final ProductRepository productRepository;
    private final OrderItemRepository orderItemRepository;
    private final DeliveryRepository deliveryRepository;

    public CustomerOrderService(CustomerOrderRepository customerOrderRepository, CustomerRepository customerRepository, AccountRepository accountRepository, ProductRepository productRepository, OrderItemRepository orderItemRepository, DeliveryRepository deliveryRepository) {
        this.customerOrderRepository = customerOrderRepository;
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
        this.productRepository = productRepository;
        this.orderItemRepository = orderItemRepository;
        this.deliveryRepository = deliveryRepository;
    }

    @Transactional
    public List<CustomerOrder> getAll(){
        List<CustomerOrder> orders = (List<CustomerOrder>) customerOrderRepository.findAll();
        for(CustomerOrder order: orders) {
            order.setOrderItems(orderItemRepository.findByOrderId(order.getId()));
            order.setCustomer(customerRepository.findByCustomerId(order.getCustomerId()));
            List<Product> productList = new ArrayList<>();
            for (OrderItem products: order.getOrderItems()) {
                Product product = productRepository.findByProductId(products.getProductId());
                productList.add(product);
            }
            order.setOrderProducts(productList);
        }

        return orders;
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(customerOrderRepository.findById(id));
    }

    public void deleteByID(int id){
        customerOrderRepository.deleteById(id);
    }

    public CustomerOrder save(CustomerOrder customerOrder){
        return customerOrderRepository.save(customerOrder);
    }

    @Transactional
    public CustomerOrder create(ListProduct listProduct, String customerToken, int deliveryId, String paymentMethod){
        LocalDate date = LocalDate.now();
        String status = "Received";
        String token = UUID.randomUUID().toString();
        Account account = accountRepository.findByToken(customerToken);
        Customer customer = customerRepository.findByAccountId(account.getAccountId());

        ArrayList<Integer> ids = listProduct.getProductIds();
        ArrayList<Integer> quantities = listProduct.getProductQuantity();

        float totalPrice = 0;

        Delivery delivery = deliveryRepository.findByDeliveryId(deliveryId);
        totalPrice += delivery.getPrice();
        for (int i: ids) {
            Product product = productRepository.findByProductId(i);
            totalPrice += product.getPrice() * quantities.get(i);
        }
        totalPrice = Math.round(totalPrice);
        customerOrderRepository.insertCustomerOrder(customer.getCustomerId(), deliveryId, paymentMethod, date, totalPrice, status, token);
        CustomerOrder customerOrder = customerOrderRepository.findByToken(token);
        for (int i: ids) {
            Product pr = productRepository.findByProductId(i);
            int quantity = quantities.get(i);
            float price = pr.getPrice() * quantity;
            orderItemRepository.insertOrderItem(customerOrder.getId(), i, quantity, price);
            Product product = productRepository.findByProductId(i);
            product.setQuantity(product.getQuantity()-quantity);
        }
        return customerOrder;
    }

    public List<CustomerOrder> getByCustomerID(int customerId) {
        return (List<CustomerOrder>) customerOrderRepository.findByCustomerId(customerId);
    }

    @Transactional
    public List<CustomerOrder> getByCustomerID(String token) {
        Account account = accountRepository.findByToken(token);
        Customer customer = customerRepository.findByAccountId(account.getAccountId());
        List<CustomerOrder> orders = customerOrderRepository.findByCustomerId(customer.getCustomerId());

        for(CustomerOrder order: orders) {
            order.setOrderItems(orderItemRepository.findByOrderId(order.getId()));

            List<Product> productList = new ArrayList<>();
            for (OrderItem products: order.getOrderItems()) {
                Product product = productRepository.findByProductId(products.getProductId());
                productList.add(product);
            }
            order.setOrderProducts(productList);
        }

        return orders;
    }

    public List<CustomerOrder> getByStatus(String status) {
        List<CustomerOrder> orders = customerOrderRepository.findByStatus(status);

        for(CustomerOrder order: orders) {
            order.setOrderItems(orderItemRepository.findByOrderId(order.getId()));
            order.setCustomer(customerRepository.findByCustomerId(order.getCustomerId()));

            List<Product> productList = new ArrayList<>();
            for (OrderItem products: order.getOrderItems()) {
                Product product = productRepository.findByProductId(products.getProductId());
                productList.add(product);
            }
            order.setOrderProducts(productList);
        }

        return orders;
    }

    public CustomerOrder changeStatus(String token) {
        CustomerOrder customerOrder = customerOrderRepository.findByToken(token);
        String status = customerOrder.getStatus();
        System.out.println(status);
        if (status.equals("Received")){
            customerOrder.setStatus("Sent");
        } else if (status.equals("Sent")){
            customerOrder.setStatus("Delivered");
        }
        customerOrderRepository.save(customerOrder);
        return customerOrder;
    }
}
