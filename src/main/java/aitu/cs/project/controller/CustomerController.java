package aitu.cs.project.controller;

import aitu.cs.project.model.Category;
import aitu.cs.project.model.Customer;
import aitu.cs.project.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer")
    public ResponseEntity<?> getAll(){
        return customerService.getAll();
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return customerService.getByID(id);
    }

    @DeleteMapping("/customer/{id}")
    public void deleteByID(@PathVariable int id){
        customerService.deleteByID(id);
    }

    @PostMapping("/customer")
    public ResponseEntity<?> create(@RequestBody Customer customer){
        return ResponseEntity.ok(customerService.save(customer));
    }

    @GetMapping("/customers/me")
    public ResponseEntity<?> getMe(@RequestHeader("auth") String token){
        return ResponseEntity.ok(customerService.getCustomerByToken(token));
    }

    @PostMapping("/registerCustomer")
    public ResponseEntity<?> register(@RequestHeader("token") String token, @RequestBody Customer customer){
        return ResponseEntity.ok(customerService.register(token, customer));
    }

}
