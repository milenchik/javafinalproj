package aitu.cs.project.controller;

import aitu.cs.project.model.Customer;
import aitu.cs.project.model.CustomerOrder;
import aitu.cs.project.model.ListProduct;
import aitu.cs.project.model.OrderItem;
import aitu.cs.project.service.CustomerOrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class CustomerOrderController {
    private final CustomerOrderService customerOrderService;

    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @GetMapping("/customerOrder")
    public List<CustomerOrder> getAll(){
        return customerOrderService.getAll();
    }

    @GetMapping("/customerOrder/status/{status}")
    public List<CustomerOrder> getByStatus(@PathVariable String status){
        return customerOrderService.getByStatus(status);
    }

    @GetMapping("/customerOrderByCustomerID")
    public List<CustomerOrder> getByCustomerID(@RequestHeader("token") String token){
        return customerOrderService.getByCustomerID(token);
    }

    @PostMapping("/sentOrder")
    public CustomerOrder changeStatus(@RequestHeader("token") String token){
        return customerOrderService.changeStatus(token);
    }

    @GetMapping("/customerOrder/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return customerOrderService.getByID(id);
    }

    @DeleteMapping("/customerOrder/{id}")
    public void deleteByID(@PathVariable int id){
        customerOrderService.deleteByID(id);
    }

    @PostMapping("/customerOrder")
    public ResponseEntity<?> save(@RequestBody CustomerOrder customerOrder){
        return ResponseEntity.ok(customerOrderService.save(customerOrder));
    }

    @PostMapping("/customerOrder/create/{deliveryId}/{paymentMethod}")
    public ResponseEntity<?> create(@PathVariable int deliveryId, @PathVariable String paymentMethod, @RequestBody ListProduct listProduct, @RequestHeader("token") String token){
        return ResponseEntity.ok(customerOrderService.create(listProduct, token, deliveryId, paymentMethod));
    }
}
