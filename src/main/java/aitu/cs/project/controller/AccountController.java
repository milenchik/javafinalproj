package aitu.cs.project.controller;

import aitu.cs.project.model.Account;
import aitu.cs.project.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/account")
    public ResponseEntity<?> getAll(){
        return accountService.getAll();
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return accountService.getByID(id);
    }

    @DeleteMapping("/account/{id}")
    public void deleteByID(@PathVariable int id){
        accountService.deleteByID(id);
    }

    @PostMapping("/account")
    public ResponseEntity<?> create(@RequestBody Account account){
        return ResponseEntity.ok(accountService.save(account));
    }

    @PostMapping("/authorization")
    public ResponseEntity<?> signIn(@RequestBody Account account){
        try {
            return ResponseEntity.ok(accountService.signIn(account));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/registration")
    public ResponseEntity<?> register(@RequestBody Account account){
        try {
            return ResponseEntity.ok(accountService.register(account));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }
}
